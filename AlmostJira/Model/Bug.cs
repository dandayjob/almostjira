﻿
namespace AlmostJira.Model
{
    using System;

    public class Bug
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime WhenOpened { get; set; }
        public bool IsOpen { get; set; }
        public int AssigneeId { get; set; }
    }
}
