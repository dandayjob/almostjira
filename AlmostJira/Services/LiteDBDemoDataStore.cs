﻿using System;
using System.Collections.Generic;
using AlmostJira.Model;
using LiteDB;
using System.Linq;

namespace AlmostJira.Services
{
    public class LiteDBDemoDataStore : IDataStore
    {
        private readonly string _connectionString;

        public LiteDBDemoDataStore(string connectionString)
        {
            _connectionString = connectionString;
            AddDemoData();
        }

        public List<BugBasic> ListOpenBugsBasic()
        {
            using (var db = new LiteRepository(_connectionString))
            {
                return db.Query<Bug>()
                    .Where(bug => bug.IsOpen)
                    .ToEnumerable()
                    .Select(bug => new BugBasic()
                    {
                        Id = bug.Id,
                        Title = bug.Title
                    })
                    .ToList();
            }
        }

        private void AddDemoData()
        {
            using (var db = new LiteRepository(_connectionString))
            {
                if (db.Query<Bug>().Where(bug => bug.IsOpen).Count() == 0)
                {
                    db.Insert<Bug>(new Bug() { Id = 1, Title = "Crashing all the time" });
                    db.Insert<Bug>(new Bug() { Id = 2, Title = "Don't like the font on this" });
                    db.Insert<Bug>(new Bug() { Id = 3, Title = "Spelling mistake" });
                    db.Insert<Bug>(new Bug() { Id = 4, Title = "Can't scroll to the bottom of the screen" });
                }
            }
        }
    }
}
