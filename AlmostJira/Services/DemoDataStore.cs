﻿using System;
using System.Collections.Generic;
using AlmostJira.Model;

namespace AlmostJira.Services
{
    public class DemoDataStore : IDataStore
    {
        private List<BugBasic> _bugs = new List<BugBasic>();

        public DemoDataStore()
        {
            _bugs.Add(new BugBasic() { Id = 1, Title = "Crashing all the time" });
            _bugs.Add(new BugBasic() { Id = 2, Title = "Don't like the font on this" });
            _bugs.Add(new BugBasic() { Id = 3, Title = "Spelling mistake" });
            _bugs.Add(new BugBasic() { Id = 4, Title = "Can't scroll to the bottom of the screen" });
        }

        public List<BugBasic> ListOpenBugsBasic()
        {
            return _bugs;
        }
    }
}
