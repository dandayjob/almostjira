﻿using System;
using System.Collections.Generic;
using AlmostJira.Model;

namespace AlmostJira.Services
{
    public interface IDataStore
    {
        List<BugBasic> ListOpenBugsBasic();
    }
}
