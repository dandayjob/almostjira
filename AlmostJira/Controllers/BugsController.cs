﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AlmostJira.Model;
using AlmostJira.Services;
using Microsoft.AspNetCore.Mvc;

namespace AlmostJira.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BugsController : ControllerBase
    {
        private readonly IDataStore _dataStore;

        public BugsController(IDataStore dataStore)
        {
            _dataStore = dataStore;
        }

        [HttpGet]
        public async Task<IEnumerable<BugBasic>> GetAsync()
        {
            return _dataStore.ListOpenBugsBasic();
        }
    }
}
