﻿import React, { Component } from 'react';
import { Button } from 'reactstrap';

export class AddNewBug extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Button color="primary">Add New Bug</Button>
        );
    }

}