﻿import React, { Component } from 'react';
import { AddNewBug } from './AddNewBug'

export class Bugs extends Component {
    static displayName = Bugs.name;

    constructor(props) {
        super(props);
        this.state = { buglist: [], loading: true };
    }

    componentDidMount() {
        this.populateBugData();
    }

    static renderBugsTable(buglist) {
        return (
            <table className='table table-striped' aria-labelledby="tabelLabel">
                <thead>
                    <tr>
                        <th>Title</th>
                    </tr>
                </thead>
                <tbody>
                    {buglist.map(bug =>
                        <tr key={bug.id}>
                            <td>{bug.title}</td>
                        </tr>
                    )}
                </tbody>
            </table>
        );
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : Bugs.renderBugsTable(this.state.buglist);

        return (
            <div>
                <h1 id="tabelLabel">Open Bugs</h1>
                {contents}
                <AddNewBug />
            </div>
        );
    }

    async populateBugData() {
        const response = await fetch('bugs');
        const data = await response.json();
        this.setState({ buglist: data, loading: false });
    }

}