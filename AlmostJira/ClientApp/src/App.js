import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Bugs } from './components/Bugs';
import { People } from './components/People';

import './custom.css'

export default class App extends Component {
  static displayName = App.name;

  render () {
    return (
      <Layout>
        <Route exact path='/' component={Bugs} />
        <Route path='/Bugs' component={Bugs} />
        <Route path='/People' component={People} />
      </Layout>
    );
  }
}
