﻿using System;
using System.Collections.Generic;
using Xunit;
using FluentAssertions;
using Moq;
using AlmostJira.Controllers;
using AlmostJira.Model;
using AlmostJira.Services;
using System.Linq;

namespace AlmostJiraTests.Controllers
{
    public class BugsControllerTests
    {
        private readonly List<BugBasic> _bugs = new List<BugBasic>();

        public BugsControllerTests()
        {
            _bugs.Add(new BugBasic() { Id = 1, Title = "Crashing all the time" });
            _bugs.Add(new BugBasic() { Id = 2, Title = "Don't like the font on this" });
            _bugs.Add(new BugBasic() { Id = 3, Title = "Spelling mistake" });
            _bugs.Add(new BugBasic() { Id = 4, Title = "Can't scroll to the bottom of the screen" });
        }

        [Fact]
        public async void CanGetBugs()
        {
            Mock<IDataStore> dataStoreMock = new Mock<IDataStore>();
            dataStoreMock.Setup(mock => mock.ListOpenBugsBasic())
                .Returns(() => { return _bugs; });

            BugsController bugsController = new BugsController(dataStoreMock.Object);

            IEnumerable<BugBasic> result = await bugsController.GetAsync();
            result.FirstOrDefault().Title.Should().Be("Crashing all the time");
        }
    }
}
